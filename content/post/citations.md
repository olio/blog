---
title: Citer
subtitle: ... ou ne pas citer ?
date: 2022-01-11
tags: ["shakespeare", "markdown"]
---

Voilà comment il est possible de citer un passage d'une œuvre.

> To be, or not to be--that is the question,  
> Whether 'tis nobler in the mind to suffer  
> The slings and arrows of outrageous fortune,  
> Or to take arms against a sea of troubles,  
> And by opposing end them. To die, to sleep --  
> No more -- and by a sleep to say we end  
> The heartache, and the thousand natural shocks  
> That flesh is heir to. 'Tis a consummation  
> Devoutly to be wished. To die, to sleep !
>
> To sleep, perchance to dream, ay, there's the rub,  
> For in that sleep of death what dreams may come  
> When we have shuffled off this mortal coil  
> Must give us pause. There's the respect  
> That makes calamity of so long life.
>
> -- <cite>[William Shakespeare][1]</cite>

[1]: https://journals.openedition.org/palimpsestes/425?file=1
 
Évidemment, vous pouvez rajouter du texte après.

Vous pouvez aussi remarquer la manière de rajouter un lien directement dans la citation,
ainsi qu'une méta-donnée supplémentaire au début du fichier : `tags`, pour donner une ou plusieurs catégories à chacun de vos articles.
